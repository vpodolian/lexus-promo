module.exports = function preload (imagesPaths, loadEndCallback) {
    var $filler = $(".preloader .filler");
    var $imgContainer = $(".preloader .img-container");    

    var progress = 0;
    var loadedCount = 0;

    for (var i in imagesPaths) {
        var img = new Image();
        img.src = imagesPaths[i];
        
        img.onload = function() { 
            loadedCount++;
            progress = loadedCount * 100 / imagesPaths.length;
            $filler.css("width", progress + "%");

            if (progress == 100) loadEndCallback();
         }

        $imgContainer.append(img);
    }
}