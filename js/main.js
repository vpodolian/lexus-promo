require("./jquery.easing.min.js");
var preload = require("./preloader.js");
require("../css/style.less");

const imagesToPreload = [
	'../images/bg_layer1_large.png',
	'../images/bg_layer2_large.jpg',
	'../images/bg_layer2.jpg',
	'../images/bg_layer1.png',
	'../images/bg_layer_clouds.jpg']

$(function() {
	preload(imagesToPreload, function() { $(".preloader").fadeOut(800); fadeInElements();});

	var scrollTimeout;
	
	var $header = $("header");
	
	// Collect sections' offsets to define the nearest section for autoscrolling 
	var sectionsInfo = [];
	$(".content > div").each(function(i, elem) {
		sectionsInfo.push({className: $(elem).attr("class"), offset: $(elem).offset().top});
	});
	
	$(window).scroll(function() {
		if ($(this).scrollTop() > $header.height())
			$header.addClass("scrolled");
		else
			$header.removeClass("scrolled");
	});

	$(window).on("mousewheel", function() {
		clearTimeout(scrollTimeout);
		
		scrollTimeout = setTimeout(function() {
			scrollToNearest(sectionsInfo);
		}, 1000);
	});

	addMenuClickHandler(".menu-video", ".video-tour .content-block");
	addMenuClickHandler(".menu-features", ".features");
	addMenuClickHandler(".menu-colors", ".colors");
	addMenuClickHandler(".header-logo", ".first");	

	scrollTo(".first");
});

function fadeInElements() {
	$("header").fadeIn(1500);
	$(".content-block").fadeIn(500);
}

function scrollToNearest(sections) {
	var top = $(window).scrollTop();
			
	if (top < sections[0].offset) {
		scrollTo("." + sections[0].className); // scroll to first block
		return;
	}
	
	for (var i = 0; i < sections.length - 1; i++) {
			if (top < sections[i+1].offset) {
				// Define nearest block to scroll to
				var prev = Math.round(Math.abs(top - sections[i].offset));
				var next = Math.round(Math.abs(top - sections[i+1].offset));
				if (prev == 0 || next == 0) return;
				if (prev < next)
					scrollTo("." + sections[i].className);
				else 
					scrollTo("." + sections[i+1].className);
				return;
		}
	}
}

function addMenuClickHandler(menuElemSelector, targetSelector) {
	$(menuElemSelector).on("click", function(e) {
		e.preventDefault();
		scrollTo(targetSelector);
	});
}

function scrollTo(selector) {
	scrollToElement($(selector));
}

function scrollToElement($target) {
	$('html, body').stop().animate({
		'scrollTop': $target.offset().top
	}, 2000, 'easeInOutCubic', function() {
		$("header").removeClass("scrolled");
	});
}