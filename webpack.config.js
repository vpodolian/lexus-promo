var path = require("path");
var webpack = require("webpack");

const PATHS = {
    app: path.join(__dirname, 'js'),
    build: path.join(__dirname, 'build'),
    style: path.join(__dirname, 'css')
};

module.exports = {
    entry: {
        app: PATHS.app + "/main.js"
    },
    output: {
        path: PATHS.build,
        filename: 'bundle.js',
        publicPath: "build/"
    },
    module: {
        loaders: [
            {
                test: /\.(jpg|png|ttf)$/, loader: "file" },
            {
                test: /\.(less|css)$/,
                loader: "style!css!less",
                include: PATHS.style
            }]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        })
    ]
};